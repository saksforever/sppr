#!/bin/bash
set -e
MODE=$1
rm -Rf ./build

if [[ $MODE == "" ]]; then

    echo "Введите матрицу, space как разделитель, enter после каждой строки"
    echo  "пример:"
    echo "1 2"
    echo "3 4"
    echo "после ввода нажмите CTRL+d"
    > /sppr/data/matrix.txt
    while read line
    do
        echo $line >> /sppr/data/matrix.txt
    done

    echo "Введите BO, ключ значение, enter после каждой строки"
    echo  "пример:"
    echo "цена 0.2"
    echo " после ввода нажмите CTRL+d"
    > /sppr/data/power.txt
    while read line
    do
        echo $line >> /sppr/data/power.txt
    done

    echo "Введите название вариантов, enter после каждой строки"
    echo  "пример:"
    echo "variant_1"
    echo "после ввода нажмите CTRL+d"
    > /sppr/data/names.txt
    while read line
    do
        echo $line >> /sppr/data/names.txt
    done

elif [[ $MODE == "divan" ]]; then
    cp /sppr/templates/divan/* /sppr/data
elif [[ $MODE == "lada" ]]; then
    cp /sppr/templates/lada/* /sppr/data
elif [[ $MODE == "job" ]]; then
    cp /sppr/templates/job/* /sppr/data
elif [[ $MODE == "custom" ]]; then
    echo "your own files"
fi

if [[ ! -s /sppr/data/matrix.txt ]] || [[ ! -s /sppr/data/power.txt ]] || [[ ! -s /sppr/data/names.txt ]]; then
    echo "some files are missing: data/matrix.txt or data/power.txt or data/names.txt"
    exit 1
fi

bo=$(awk 'END{print NR}' data/matrix.txt)
variables=$(awk -F' ' '{print NF; exit}' data/matrix.txt)


sed -i "s/const\ int\ VARIANT\ =.*/const\ int\ VARIANT\ = ${variables};/" /sppr/main.cpp
sed -i "s/const\ int\ BO\ =.*/const\ int\ BO\ = ${bo};/" /sppr/main.cpp

mkdir build && cd build
cmake .. > /dev/null && make > /dev/null
cp sppr ../
cd ..
./sppr