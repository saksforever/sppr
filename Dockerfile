FROM ubuntu:22.04

RUN apt update && apt install -y build-essential cmake

WORKDIR /sppr

COPY . .

RUN mkdir -p data

ENTRYPOINT ["/sppr/entrypoint.sh"]

